from flask import Blueprint
from flask_restful import Resource, Api

from app.helpers.files import load_file


class FileResource(Resource):
    def get(self,container,filename):
        return load_file(container,filename)

bp = Blueprint('file',__name__)
bp_api = Api(bp)
bp_api.add_resource(FileResource,'/<string:container>/<string:filename>')
