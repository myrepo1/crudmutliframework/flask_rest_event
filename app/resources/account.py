"""
Resource for account API.
All Account API has endpoint started with '/accounts'.
"""

from datetime import timedelta
from flask import Blueprint, request
from flask_jwt_extended import create_access_token, fresh_jwt_required, get_jwt_identity
from marshmallow import fields

from app import db
from app.helpers.files import remove_file, save_file, generate_unique_name
from app.helpers.general import generate_filter, get_current_host
from app.helpers.responses import unauthorized_response, resource_not_found, validation_failed
from app.models import Account
from . import CustomApi,CustomResource

schema = {
    'data_id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True,
        'post_required':False,
        },
    'username':{
        'field':fields.String,
        'options':{'allow_none':False},
        'is_payload':True,
        'is_query':True,
        'post_required':True,
    },
    'password':{
        'field':fields.String,
        'options':{'allow_none':False,'load_only':True},
        'is_payload':True,
        'post_required':True
    },
    'name':{
        'field':fields.String,
        'options':{'allow_none':False},
        'is_payload':True,
        'is_query':True,
        'post_required':False,
    },
    'phone':{
        'field':fields.String,
        'options':{'allow_none':True},
        'is_payload':True,
        'is_query':True,
        'post_required':False,
    },
    'is_staff':{
        'field':fields.Boolean,
        'options':{'allow_none':False},
        'is_payload':True,
        'is_query':True,
        'post_required':False
    },
    'profpic':{
        'field':fields.String,
        'options':{'dump_only':True},
        'is_payload':True,
    },
    'has_profpic':{
        'field':fields.Boolean,
        'options':{},
        'is_query':True
    }
}

login_schema = {
    'username':{
        'field':fields.String,
        'options':{'allow_none':False,'load_only':True},
        'is_payload':True,
        'post_required':True
    },
    'password':{
        'field':fields.String,
        'options':{'allow_none':False,'load_only':True},
        'is_payload':True,
        'post_required':True,
    },
    'access_token':{
        'field':fields.String,
        'options':{'dump_only':True},
        'is_payload':True
    },
    'refresh_token':{
        'field':fields.String,
        'options':{'dump_only':True},
        'is_payload':True
    },
    'id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True
    },
    'is_staff':{
        'field':fields.Boolean,
        'options':{'dump_only':True},
        'is_payload':True
    },
}


class AccountResource(CustomResource):
    """
    Account resource for endpoint '/' or endpoint with no url parameters.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def get(self):
        """
        Get accounts data. Only account with is_staff true is allowed to access this
        resource.
        """
        user = get_jwt_identity()
        if not user['is_staff']:
            return self.get_response(**unauthorized_response)
        query = self.get_cleaned_query()
        if any(self.errors):
            return self.get_response(**validation_failed,message='Query Validation Failed.')
        filter_query = generate_filter(Account,query)
        if query.get('has_profpic',False):
            filter_query.append(Account.profpic!=None)
        records = Account.query.filter(*filter_query).all()
        return self.get_response(data=records)

    def post(self):
        """
        Create one account data.
        """
        data = self.get_cleaned_data(is_post=True)
        if any(self.errors):
            return self.get_response(**validation_failed)
        if Account.query.filter_by(username=data['username']).first():
            self.add_error('username',f'Username {data["username"]} already exist.')
        if data.get('phone','') and not data['phone'].isnumeric():
            self.add_error('phone',f'Phone {data["phone"]} is not a valid phone number.')
        if any(self.errors):
            return self.get_response(**validation_failed)
        account = Account(**data)
        account.set_password(data['password'])
        db.session.add(account)
        db.session.commit()
        return self.get_response(data=account, code=201)


class AccountDetailResource(CustomResource):
    """
    Account resource with endpoint /{data_id} or with url parameters account's id.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def get(self,data_id):
        """
        Get one account data based on data_id.
        """
        user = get_jwt_identity()
        if user['id'] != data_id and not user['is_staff']:
            return self.get_response(**unauthorized_response)
        account = Account.query.filter(Account.data_id==data_id).first()
        if not account:
            return self.get_response(**resource_not_found)
        return self.get_response(data=account)

    @fresh_jwt_required
    def put(self,data_id):
        """
        Update one account data based on data_id.
        """
        user = get_jwt_identity()
        if user['id'] != data_id and not user['is_staff']:
            return self.get_response(**unauthorized_response)
        account = Account.query.filter(Account.data_id==data_id).first()
        if not account:
            return self.get_response(**resource_not_found)
        data = self.get_cleaned_data()
        if any(self.errors):
            return self.get_response(**validation_failed)
        if data.get('username','') and data['username'] != account.username \
            and Account.query.filter_by(username=data['username']):
            self.add_error('username',f'Username {data["username"]} already exist.')
        if data.get('phone','') and not data['phone'].isnumeric():
            self.add_error('phone',f'Phone {data["phone"]} is not a valid phone number.')
        if any(self.errors):
            return self.get_response(**validation_failed)
        for k, v in data.items():
            setattr(account, k, v)
        db.session.commit()
        return self.get_response(data=account)

    @fresh_jwt_required
    def delete(self,data_id):
        """
        Delete one account based on its data_id.
        """
        user = get_jwt_identity()
        if user['id'] != data_id and not user['is_staff']:
            return self.get_response(**unauthorized_response)
        account = Account.query.filter(Account.data_id==data_id).first()
        if not account:
            return self.get_response(**resource_not_found)
        if account.profpic:
            remove_file(account.profpic)
        db.session.delete(account)
        db.session.commit()
        return self.get_response(message=f'Account with id {data_id} is deleted succsessfully.')

class AccountImageResource(CustomResource):
    """
    Account API for add image/profpic feature.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def post(self,data_id):
        """
        Add image for an account based on its data_id.
        """
        user = get_jwt_identity()
        if user['id'] != data_id and not user['is_staff']:
            return self.get_response(**unauthorized_response)
        account = Account.query.filter(Account.data_id==data_id).first()
        if not account:
            return self.get_response(**resource_not_found)
        profpic_file = request.files.get('profpic',None)
        if not profpic_file:
            self.add_error('profpic','Profpic Image Not Found.')
            return self.get_response(code=400,status=False,message='Validation Failed.')
        if account.profpic:
            remove_file(account.profpic)
        filename = generate_unique_name(profpic_file.filename)
        profpic_dir = save_file(profpic_file,'profpic',filename)
        profpic_url = get_current_host()+'files/'+profpic_dir
        setattr(account,'profpic',profpic_url)
        db.session.commit()
        return self.get_response(
            code=200,status=True,message="Account's profpic successfully uploaded.",data=account)

class AccountLoginResource(CustomResource):
    """
    Account resource for login feature with endpoint /login.
    """

    def __init__(self):
        super().__init__(login_schema)

    def generate_access_token(self,data):
        """
        Generate access_token using jwt create_access_token method.
        Parameters:
        ------
        data:dict
            Data to be saved on access_token.
        Return: str
        ------
            Return generated access_token.
        """
        expires = timedelta(days=1)
        access_token = create_access_token(data,fresh=True,expires_delta=expires)
        return access_token

    def post(self):
        """
        Login or get access_token using username and password.
        """
        data = self.get_cleaned_data(is_post=True)
        if any(self.errors):
            return self.get_response(**validation_failed)
        username = data['username']
        password = data['password']
        account = Account.query.filter_by(username=username).first()
        if not account or not account.check_password(password):
            self.add_error(None,'Wrong Username or Password.')
            return self.get_response(**validation_failed)
        data = {'id':account.data_id,'is_staff':account.is_staff}
        access_token = self.generate_access_token(data)
        result = {'access_token':access_token,**data}
        return self.get_response(data=result,message='Login success.')


bp = Blueprint('account',__name__)
bp_api = CustomApi(bp)
bp_api.add_resource(AccountResource,'')
bp_api.add_resource(AccountDetailResource,'/<int:data_id>')
bp_api.add_resource(AccountImageResource,'/<int:data_id>/image')
bp_api.add_resource(AccountLoginResource,'/login')
