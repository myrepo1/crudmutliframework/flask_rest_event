"""
Resource for Event API.
All event API has endpoint started with '/events'.
"""

from datetime import datetime
from flask import Blueprint, request
from flask_jwt_extended import fresh_jwt_required, get_jwt_identity, jwt_optional
from marshmallow import fields
from sqlalchemy import and_
from sqlalchemy.orm import contains_eager, joinedload

from app import db
from app.helpers.files import remove_file, save_file, generate_unique_name
from app.helpers.general import get_current_host
from app.helpers.responses import unauthorized_response, resource_not_found, validation_failed
from app.models import Event, EventInvolved
from . import CustomApi,CustomResource

involved_schema = {
    'data_id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True,
        },
    'account_id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True,
    },
    'event_id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True,
    },
    'name':{
        'field':fields.String,
        'options':{'dump_only':True},
        'is_payload':True
    }
}

schema = {
    'data_id':{
        'field':fields.Integer,
        'options':{'dump_only':True},
        'is_payload':True,
        },
    'name':{
        'field':fields.String,
        'options':{'allow_none':False},
        'is_payload':True,
        'is_query':True,
        'post_required':True,
    },
    'shortname':{
        'field':fields.String,
        'options':{'allow_none':True},
        'is_payload':True,
        'is_query':True,
    },
    'start':{
        'field':fields.DateTime,
        'options':{'format':'%d-%m-%Y %H:%M','allow_none':False},
        'is_payload':True,
        'is_query':True,
        'post_required':True,
    },
    'end':{
        'field':fields.DateTime,
        'options':{'format':'%d-%m-%Y %H:%M','allow_none':True},
        'is_payload':True,
        'is_query':True,
    },
    'description':{
        'field':fields.String,
        'options':{'allow_none':True},
        'is_payload':True,
    },
    'location':{
        'field':fields.String,
        'options':{'allow_none':True},
        'is_payload':True,
        'is_query':True
    },
    'preview':{
        'field':fields.String,
        'options':{'dump_only':True},
        'is_payload':True
    },
    'involved':{
        'field':fields.Nested,
        'options':{},
        'source':involved_schema,
        'nested':True,
        'many':True,
        'is_payload':True,
        'additional':True
    },
    'view_joined':{
        'field':fields.Boolean,
        'options':{},
        'is_query':True,
    },
    'include_account':{
        'field':fields.Boolean,
        'options':{},
        'is_query':True
    }
}

class EventResource(CustomResource):
    """
    Event resource for endpoint '/' or endpoint with no url parameters.
    """
    def __init__(self):
        super().__init__(schema)

    @jwt_optional
    def get(self):
        """
        Get Event data, jwt is optional.
        """
        user = get_jwt_identity()
        query = self.get_cleaned_query()
        if any(self.errors):
            return self.get_response(**validation_failed,message='Query Validation Failed.')
        if query.get('view_joined') and user:
            sql_query = Event.query.outerjoin(
                EventInvolved,
                and_(Event.data_id==EventInvolved.event_id,EventInvolved.account_id==user['id'])
                )
            sql_query = sql_query.options(contains_eager(Event.involved))
            records = sql_query.all()
            record_list = []
            for item in records:
                dict_item = item.__dict__
                involved_list = []
                for involve in dict_item['involved']:
                    involved_list.append(involve.__dict__)
                dict_item['involved'] = involved_list
                record_list.append(dict_item)
            return self.get_response(data=record_list, message='Events successfully retreived.')
        else:
            self.schema['involved']['is_payload'] = False
            records = Event.query.all()
            return self.get_response(data=records,message='Events successfully retreived.')

    @fresh_jwt_required
    def post(self):
        """
        Create an event. Only staff allowed to create event.
        """
        user = get_jwt_identity()
        if not user['is_staff']:
            return self.get_response(**unauthorized_response)
        data = self.get_cleaned_data(is_post=True)
        if any(self.errors):
            return self.get_response(**validation_failed)
        if Event.query.filter(Event.name==data['name']).first():
            self.add_error('name',f'Name {data["name"]} already exist.')
        if data.get('end',None):
            if data['end'] < data['start']:
                self.add_error(None,'Start must be before End.')
        if any(self.errors):
            return self.get_response(**validation_failed)
        event = Event(**data)
        db.session.add(event)
        db.session.commit()
        return self.get_response(data=event,message='Event successfully created.', code=201)


class EventDetailResource(CustomResource):
    """
    Event Resource for end point '/{data_id}' or endpoint with url parameters data_id.
    """

    def __init__(self):
        super().__init__(schema)

    @jwt_optional
    def get(self,data_id):
        """
        Get single event data based data_id.
        """
        user = get_jwt_identity()
        query = self.get_cleaned_query()
        if user and user['is_staff'] and query.get('include_account',False):
            sql_query = Event.query.options(
                joinedload(Event.involved).joinedload(EventInvolved.account))
            sql_query = sql_query.filter(Event.data_id==data_id)
            result = sql_query.all()
            result_dict = result[0].__dict__
            involved = []
            for item in result_dict['involved']:
                raw_involved = item.__dict__
                involved_account = raw_involved.pop('account')
                converted_involved = {**raw_involved,**involved_account.__dict__}
                involved.append(converted_involved)
            result_dict['involved'] = involved
            return self.get_response(data=result_dict,message='Event successfully retreived.')
        else:
            event = Event.query.filter(Event.data_id==data_id).first()
            if not event:
                return self.get_response(**resource_not_found)
            return self.get_response(data=event,message='Event successfully retreived.')

    @fresh_jwt_required
    def put(self,data_id):
        """
        Edit one event based on its data_id.
        Only staff allowed to edit event.
        """
        user = get_jwt_identity()
        if not user['is_staff']:
            return self.get_response(**unauthorized_response)
        event = Event.query.filter(Event.data_id==data_id).first()
        if not event:
            return self.get_response(**resource_not_found)
        data = self.get_cleaned_data()
        if any(self.errors):
            return self.get_response(**validation_failed)
        if data.get('name','') and data['name'] != event.name and \
            Event.query.filter(Event.name==data['name']).first():
            self.add_error('name',f'Name {data["name"]} already exist.')
        if data.get('start',None) and data.get('end',None):
            if data['end'] < data['start']:
                self.add_error(None,'Start must be before End.')
        if any(self.errors) or None in self.errors:
            return self.get_response(**validation_failed)
        for k,v in data.items():
            setattr(event,k,v)
        db.session.commit()
        return self.get_response(data=event,message='Events successfully updated.')

    @fresh_jwt_required
    def delete(self,data_id):
        """
        Delete one event based on its data_id.
        Only staff allowed to delete event.
        """
        user = get_jwt_identity()
        if not user['is_staff']:
            return self.get_response(**unauthorized_response)
        event = Event.query.filter(Event.data_id==data_id).first()
        if not event:
            return self.get_response(**resource_not_found)
        if event.preview:
            remove_file(event.preview)
        db.session.delete(event)
        db.session.commit()
        return self.get_response(message=f'Event with id {data_id} is succsessfully deleted.')


class EventImageResource(CustomResource):
    """
    Event resource for add event's preview image.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def post(self,data_id):
        """
        Add event preview image.
        Only staff allowed to add preview.
        Only one preview may exist for one event.
        """
        user = get_jwt_identity()
        if not user['is_staff']:
            return self.get_response(**unauthorized_response)
        event = Event.query.filter(Event.data_id==data_id).first()
        if not event:
            return self.get_response(**resource_not_found)
        preview_file = request.files.get('preview',None)
        if not preview_file:
            self.add_error('preview','Preview Image Not Found.')
            return self.get_response(**validation_failed)
        if event.preview:
            remove_file(event.preview)
        filename = generate_unique_name(preview_file.filename)
        preview_dir = save_file(preview_file,'preview',filename)
        preview_url = get_current_host()+'files/'+ preview_dir
        event.preview = preview_url
        db.session.commit()
        return self.get_response(message='Event preview successfully uploaded.',data=event)


class EventJoinResource(CustomResource):
    """
    Event resource for join event feature.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def post(self,data_id):
        """
        Join an event, based on its data_id.
        Account who join is retreived from jwt identity.
        """
        user = get_jwt_identity()
        event = Event.query.filter(Event.data_id==data_id).first()
        if not event:
            return self.get_response(**resource_not_found)
        if event.start < datetime.utcnow():
            self.add_error(None,'Event has passed.')
        involved_event = EventInvolved.query.filter(
            EventInvolved.event_id==event.data_id,EventInvolved.account_id==user['id']).first()
        if involved_event:
            self.add_error(None,'Event already joined.')
        if any(self.errors):
            return self.get_response(**validation_failed)
        involved_event = EventInvolved(event_id=event.data_id,account_id=user['id'])
        db.session.add(involved_event)
        db.session.commit()
        return self.get_response(message='Event joined successfully.')


class EventCancelResource(CustomResource):
    """
    Event resource for cancel joining event feature.
    """
    def __init__(self):
        super().__init__(schema)

    @fresh_jwt_required
    def post(self,data_id):
        """
        Cancel joining an event, based on its data_id.
        Account who join is retreived from jwt identity.
        """
        user = get_jwt_identity()
        event = Event.query.filter(Event.data_id==data_id).first()
        if not event:
            return self.get_response(**resource_not_found)
        involved_event = EventInvolved.query.filter(
            EventInvolved.event_id==event.data_id,EventInvolved.account_id==user['id']).first()
        if not involved_event:
            self.add_error(None,'Account not yet join event.')
        if any(self.errors):
            return self.get_response(**validation_failed)
        db.session.delete(involved_event)
        db.session.commit()
        return self.get_response(message='Event canceled successfully.')


bp = Blueprint('event',__name__)
bp_api = CustomApi(bp)
bp_api.add_resource(EventResource,'')
bp_api.add_resource(EventDetailResource,'/<int:data_id>')
bp_api.add_resource(EventImageResource,'/<int:data_id>/image')
bp_api.add_resource(EventJoinResource,'/<int:data_id>/join')
bp_api.add_resource(EventCancelResource,'/<int:data_id>/cancel')
