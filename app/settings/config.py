import os
from pathlib import Path
from dotenv import load_dotenv

BASEDIR = Path(os.path.join(os.path.dirname(__file__))).parent.parent
load_dotenv(os.path.join(BASEDIR,'.flaskenv'))

class Config(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///'+os.path.join(BASEDIR,'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MEDIA_DIR = 'media'
    JWT_SECRET_KEY = 'c7a7c494c7eba445a3d5596d63b849e9'

class TestConfig(object):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_TEST_URI') or 'sqlite:///'+os.path.join(BASEDIR,'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MEDIA_DIR = 'test_media'
    JWT_SECRET_KEY = 'kalsjd19203j312k3lj1k2l3'