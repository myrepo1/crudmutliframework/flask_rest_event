"""
List of models representing event's table.
"""

from werkzeug.security import check_password_hash, generate_password_hash

from app import db
from app.helpers.general import generate_utc_time

class Event(db.Model):
    """
    Event model for store event data in database.
    """
    data_id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(150),nullable=False)
    shortname = db.Column(db.String(15), nullable=True)
    start = db.Column(db.DateTime,default=generate_utc_time,nullable=False)
    end = db.Column(db.DateTime, nullable=True)
    description =  db.Column(db.Text, nullable=True)
    location = db.Column(db.String(150), nullable=True)
    preview = db.Column(db.String(200), nullable=True)
    involved = db.relationship(
        'EventInvolved',backref='event',cascade='all, delete', passive_deletes=True)

    def __repr__(self):
        name = self.shortname if self.shortname else self.name
        return f'<Event {name}>'

class Account(db.Model):
    """
    Account model for store account data in database.
    """

    data_id = db.Column(db.Integer,primary_key=True)
    username = db.Column(db.String(150),nullable=False)
    password = db.Column(db.String(150),nullable=False)
    name = db.Column(db.String(150),nullable=False)
    phone = db.Column(db.String(20), nullable=True)
    is_staff = db.Column(db.Boolean,default=False,nullable=False)
    profpic = db.Column(db.String(200), nullable=True)
    involved = db.relationship('EventInvolved',backref='account',passive_deletes=True)

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self,value):
        """
        Set account password using werkzeug hashing method.
        Parameters:
        ------
        value: str
            Value to be hashed
        """
        self.password = generate_password_hash(value)

    def check_password(self,value):
        """
        Check value equal to current account's password.
        Parameters:
        ------
            Value to be checked.
        Return: bool
        """
        return check_password_hash(self.password,value)

class EventInvolved(db.Model):
    """
    EventInvolved model for store account that involved or join an event.
    """
    data_id = db.Column(db.Integer,primary_key=True)
    event_id = db.Column(
        db.Integer,db.ForeignKey('event.data_id', ondelete='CASCADE'), nullable=False)
    account_id = db.Column(
        db.Integer,db.ForeignKey('account.data_id', ondelete='SET NULL'), nullable=True)
