import unittest
import json
import os
from datetime import timedelta
from shutil import rmtree

from app import db
from app.helpers.general import generate_utc_time
from app.helpers.files import generate_full_filepath, check_file_exist
from app.models import Account,Event, EventInvolved
from app.resources.account import AccountLoginResource
from app.run import run_test_app


class BaseCase(unittest.TestCase):
    def setUp(self):
        self.app = run_test_app()
        self.client = self.app.test_client()
        self.db = db
        self.account = {}
        self.event = {}
        self.involved = {}
        with self.app.app_context():
            self.db.drop_all()
            self.db.create_all()

    def tearDown(self):
        with self.app.app_context():
            media_path = generate_full_filepath('')
            if check_file_exist(media_path):
                rmtree(media_path)

    def generate_account(self, data=None):
        with self.app.app_context():
            if data:
                account = Account(**data)
                account.set_password(data['password'])
                self.db.session.add(account)
                self.db.session.commit()
                return account.data_id
            else:
                self.account['username'] = 'test'
                self.account['password'] = 'test'
                self.account['is_staff'] = True
                data = {
                    'username':self.account['username'],
                    'password':self.account['password'],
                    'name':'abcsa',
                    'is_staff':self.account['is_staff']
                    }
                account = Account(**data)
                account.set_password(self.account['password'])
                self.db.session.add(account)
                self.db.session.commit()
                self.account['data_id'] =  account.data_id
                

    def generate_event(self, data=None):
        with self.app.app_context():
            if not data:
                self.event['name'] = 'Test Event'
                self.event['start'] = generate_utc_time() + timedelta(days=10)
                data = {'name':self.event['name'],'start':self.event['start']}
                event = Event(**data)
                self.db.session.add(event)
                self.db.session.commit()
                self.event['data_id'] = event.data_id
            else:
                event = Event(**data)
                self.db.session.add(event)
                self.db.session.commit()
                return event.data_id

    def generate_involvement(self,data=None):
        with self.app.app_context():
            if not data:
                self.involved['event_id'] = self.event['data_id']
                self.involved['account_id'] = self.account['data_id']
                involved = EventInvolved(**self.involved)
                self.db.session.add(involved)
                self.db.session.commit()
                self.involved['data_id'] = involved.data_id
            else:
                involved = EventInvolved(**data)
                self.db.session.add(involved)
                self.db.session.commit()
                return involved.data_id

    def generate_access_token(self,payload=None):
        with self.app.app_context():
            login_resource = AccountLoginResource()
            if payload:
                return login_resource.generate_access_token(payload)
            else:
                data = {'id':self.account['data_id'],'is_staff':self.account['is_staff']}
                return login_resource.generate_access_token(data)

    def get_data(self,url,use_auth=False,query_string={}):
        headers = {}
        if use_auth:
            access_token = self.generate_access_token()
            headers['Authorization'] = 'Bearer ' + access_token
        return self.client.get(url,headers=headers, query_string=query_string)

    def post_data(self,url,payload,use_auth=True):
        headers={'Content-Type':'application/json'}
        if use_auth:
            access_token = self.generate_access_token()
            headers['Authorization'] = 'Bearer '+access_token
        return self.client.post(url,headers=headers,json=payload)

    def post_non_json_data(self,url,data,use_auth=True):
        headers = {'Content-Type':'multipart/form-data'}
        if use_auth:
            access_token = self.generate_access_token()
            headers['Authorization'] = 'Bearer '+access_token
        return self.client.post(url,data=data,headers=headers)

    def put_data(self,url,payload,use_auth=True):
        headers={'Content-Type':'application/json'}
        if use_auth:
            access_token = self.generate_access_token()
            headers['Authorization'] = 'Bearer '+access_token
        return self.client.put(url,headers=headers,json=payload)

    def delete_data(self,url,use_auth=True):
        headers = {}
        if use_auth:
            access_token = self.generate_access_token()
            headers['Authorization'] = 'Bearer '+access_token
        return self.client.delete(url,headers=headers)