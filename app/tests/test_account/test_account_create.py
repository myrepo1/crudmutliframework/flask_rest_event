from app.tests import BaseCase

class TestAccountCreate(BaseCase):
    def test_success_account_create(self):
        payload = {'username':'testuser','password':'testuser','name':'testname'}
        response = self.post_data('/accounts',payload,use_auth=False)
        self.assertEqual(response.status_code,201)
        self.assertEqual(response.json['status'],True)
        self.assertIn('username',response.json['data'])
        self.assertEqual(response.json['data']['username'],'testuser')
        self.assertIn('data_id',response.json['data'])
        self.assertNotIn('password',response.json['data'])

    def test_failed_account_create_empty_field(self):
        payload = {'username':'','password':'','name':''}
        response = self.post_data('/accounts',payload,use_auth=False)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('username',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['username'])
        self.assertIn('password',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['password'])
        self.assertIn('name',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['name'])

    def test_failed_account_create_invalid_value(self):
        payload = {'username':'aklasdasd','password':'kalsdasd','name':'kl123klasd','phone':'123a'}
        response = self.post_data('/accounts',payload,use_auth=False)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('phone',response.json['detail_message'])
        self.assertIn(f'Phone {payload["phone"]} is not a valid phone number.',response.json['detail_message']['phone'])

    def test_failed_account_create_user_already_exist(self):
        self.generate_account()
        payload = {'username':self.account['username'],'password':'askdlasd','name':'akl123'}
        response = self.post_data('/accounts',payload,use_auth=False)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('username',response.json['detail_message'])
        self.assertIn(f'Username {self.account["username"]} already exist.',response.json['detail_message']['username'])
