from app.tests import BaseCase

class TestAccountGetOne(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_account_get_one(self):
        url = '/accounts/' + str(self.account['data_id'])
        response = self.get_data(url,use_auth=True)
        self.assertEqual(response.status_code,200)
        self.assertIn('data_id',response.json['data'])
        self.assertNotIn('password',response.json['data'])
        self.assertIn('username',response.json['data'])
        self.assertEqual(response.json['data']['username'],self.account['username'])

    def test_failed_account_get_one_no_token(self):
        url = '/accounts/' + str(self.account['data_id'])
        response = self.get_data(url)
        self.assertEqual(response.status_code,401)

    def test_failed_account_get_account_not_found(self):
        url = '/accounts/' + str(self.account['data_id']+10)
        response = self.get_data(url, use_auth=True)
        self.assertEqual(response.status_code,404)