from os import path
from urllib.parse import urlparse
from app.tests import BaseCase

class TestAccountUploadProfpic(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        file_path = 'seeders/static/profpic.jpeg'
        root_path = self.app.root_path
        upper_root_path = '/'.join(root_path.split('/')[:-1])
        self.full_path = path.join(upper_root_path,file_path)
    
    def test_success_account_upload_file(self):
        data = {'profpic':open(self.full_path,'rb')}
        url = '/accounts/'+str(self.account['data_id'])+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.json['status'],True)
        self.assertIn('profpic',response.json['data'])
        self.assertIsNotNone(response.json['data']['profpic'])
        parse_result = urlparse(response.json['data']['profpic'])
        self.assertNotEqual(parse_result.scheme,'')
        self.assertNotEqual(parse_result.netloc,'')

    def test_failed_account_upload_no_token(self):
        data = {'profpic':open(self.full_path,'rb')}
        url = '/accounts/'+str(self.account['data_id'])+'/image'
        response = self.post_non_json_data(url,data, use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_account_upload_account_not_found(self):
        data = {'profpic':open(self.full_path,'rb')}
        url = '/accounts/'+str(self.account['data_id']+10)+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,404)

    def test_failed_account_upload_no_file(self):
        data = {'profpic':''}
        url = '/accounts/'+str(self.account['data_id'])+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('profpic',response.json['detail_message'])
        self.assertIn('Profpic Image Not Found.',response.json['detail_message']['profpic'])