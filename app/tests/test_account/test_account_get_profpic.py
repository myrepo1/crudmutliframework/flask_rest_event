from os import path
from app.tests import BaseCase

class TestUserGetProfpic(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        file_path = 'seeders/static/profpic.jpeg'
        root_path = self.app.root_path
        upper_root_path = '/'.join(root_path.split('/')[:-1])
        self.full_path = path.join(upper_root_path,file_path)
    
    def test_success_account_get_profpic(self):
        data = {'profpic':open(self.full_path,'rb')}
        url = '/accounts/'+str(self.account['data_id'])+'/image'
        response = self.post_non_json_data(url,data)
        profpic_url = response.json['data']['profpic']
        response = self.client.get(profpic_url)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.content_type,'image/jpeg')