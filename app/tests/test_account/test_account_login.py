from app.tests import BaseCase

class TestUserLogin(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_account_login(self):
        payload = {'username':self.account['username'],'password':self.account['password']}
        response = self.post_data('/accounts/login',payload,use_auth=False)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.json['status'],True)
        self.assertIn('id',response.json['data'])
        self.assertEqual(response.json['data']['id'],self.account['data_id'])
        self.assertIn('is_staff',response.json['data'])
        self.assertEqual(response.json['data']['is_staff'],self.account['is_staff'])
        self.assertIn('access_token',response.json['data'])
    
    def test_failed_account_login_empty_field(self):
        payload = {'username':'','password':''}
        response = self.post_data('/accounts/login',payload,use_auth=False)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('username',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['username'])
        self.assertIn('password',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['password'])

    def test_failed_account_login_wrong_username_password(self):
        payload = {'username':'asdasd','password':'asdasd'}
        response = self.post_data('/accounts/login',payload,use_auth=False)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Wrong Username or Password.',response.json['detail_message']['__all__'])