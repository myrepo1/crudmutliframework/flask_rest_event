from app.models import Account
from app.tests import BaseCase

class TestAccountDelete(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_account_delete(self):
        url = '/accounts/' + str(self.account['data_id'])
        response = self.delete_data(url)
        self.assertEqual(response.status_code,200)
        account_found = True
        with self.app.app_context():
            account = Account.query.filter(Account.data_id==self.account['data_id']).first()
            if not account:
                account_found = False
        self.assertFalse(account_found)

    def test_failed_account_delete_no_token(self):
        url = '/accounts/' + str(self.account['data_id'])
        response = self.delete_data(url,use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_account_delete_account_not_found(self):
        url = '/accounts/' + str(self.account['data_id']+10)
        response = self.delete_data(url)
        self.assertEqual(response.status_code,404)