from app.tests import BaseCase

class TestAccountEdit(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_account_edit(self):
        url = '/accounts/' + str(self.account['data_id'])
        payload = {'name':'edited name'}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,200)
        self.assertIn('data_id',response.json['data'])
        self.assertNotIn('password',response.json['data'])
        self.assertIn('name',response.json['data'])
        self.assertEqual(response.json['data']['name'],payload['name'])

    def test_failed_account_edit_no_token(self):
        url = '/accounts/' + str(self.account['data_id'])
        payload = {'name':'edited name'}
        response = self.put_data(url,payload, use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_account_edit_account_not_found(self):
        url = '/accounts/' + str(self.account['data_id']+10)
        payload = {'name':'edited name'}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,404)

    def test_failed_account_edit_empty_data(self):
        url = '/accounts/' + str(self.account['data_id'])
        payload = {'username':'','password':'','name':''}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('username',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['username'])
        self.assertIn('password',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['password'])
        self.assertIn('name',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['name'])

    def test_failed_account_invalid_data(self):
        existing_account = {'username':'account1','password':'account1','name':'abcde'}
        self.generate_account(data=existing_account)
        url = '/accounts/' + str(self.account['data_id'])
        payload = {'username':existing_account['username'],'phone':'asdasd123'}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('username',response.json['detail_message'])
        self.assertIn(f'Username {payload["username"]} already exist.',response.json['detail_message']['username'])
        self.assertIn('phone',response.json['detail_message'])
        self.assertIn(f'Phone {payload["phone"]} is not a valid phone number.',response.json['detail_message']['phone'])