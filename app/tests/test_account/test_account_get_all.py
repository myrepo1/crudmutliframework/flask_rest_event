from datetime import timedelta
from flask_jwt_extended import create_access_token

from app.tests import BaseCase

class TestAccountGetAll(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_account_get_all(self):
        response = self.get_data('/accounts',use_auth=True)
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertIn('data_id',response.json['data'][0])
        self.assertNotIn('password',response.json['data'][0])
        self.assertIn('username',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['username'],self.account['username'])

    def test_failed_account_get_unaunthorized(self):
        response = self.get_data('/accounts')
        self.assertEqual(response.status_code,401)
    
    def test_failed_account_get_all_wrong_token_format(self):
        headers = {'Authorization':'aaasdasdasd'}
        response = self.client.get('/accounts',headers=headers)
        self.assertEqual(response.status_code,422)

    def test_failed_account_get_all_wrong_token(self):
        headers = {'Authorization':'Bearer aksdlasdasd'}
        response = self.client.get('/accounts',headers=headers)
        self.assertEqual(response.status_code,422)

    def test_failed_account_get_all_expired_token(self):
        token_data = {'id':self.account['data_id'],'is_staff':self.account['is_staff']}
        expires_delta = timedelta(days=-2)
        headers = {}
        with self.app.app_context():
            access_token = create_access_token(token_data,fresh=True,expires_delta=expires_delta)
            headers['Authorization'] = 'Bearer ' + access_token
        response = self.client.get('/accounts',headers=headers)
        self.assertEqual(response.status_code,401)

    def test_failed_account_get_all_non_staff_account(self):
        new_account = {'username':'new','password':'new','name':'New','is_staff':False}
        account_id = self.generate_account(data=new_account)
        access_token = self.generate_access_token({'id':account_id,'is_staff':new_account['is_staff']})
        headers = {'Authorization':'Bearer '+access_token}
        response = self.client.get('/accounts',headers=headers)
        self.assertEqual(response.status_code,403)


class TestAccountGetAllWithQuery(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
    
    def success_account_get_all_with_query_equal(self):
        more_account_data = {'username':'test1','password':'more','name':'More','is_staff':False}
        self.generate_account(data=more_account_data)
        query = {'username':'test'}
        response = self.get_data('/accounts',use_auth=True,query_string=query)
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertEqual(len(response.json['data']),1)
        self.assertIn('username',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['username'],self.account['username'])

    def success_account_get_all_with_query_ilike(self):
        more_account_data = {'username':'test1','password':'more','name':'More','is_staff':False}
        self.generate_account(data=more_account_data)
        query = {'username__ilike':'test'}
        response = self.get_data('/accounts',use_auth=True,query_string=query)
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertEqual(len(response.json['data']),2)
        self.assertIn('username',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['username'],self.account['username'])
        self.assertIn('username',response.json['data'][1])
        self.assertEqual(response.json['data'][1]['username'],more_account_data['username'])

    def failed_account_get_all_wrong_query(self):
        more_account_data = {'username':'test1','password':'more','name':'More','is_staff':False}
        self.generate_account(data=more_account_data)
        query = {'is_staff':2}
        response = self.get_data('/accounts',use_auth=True,query_string=query)
        self.assertEqual(response.status_code,400)
        self.assertEqual(response.data.message,'Query Validation Failed.')