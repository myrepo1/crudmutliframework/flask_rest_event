from app.tests import BaseCase

class TestEventGetOne(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_event()

    def test_success_event_get_one(self):
        url = '/events/' + str(self.event['data_id'])
        response = self.get_data(url)
        self.assertEqual(response.status_code,200)
        self.assertIn('data_id',response.json['data'])
        self.assertEqual(response.json['data']['data_id'],self.event['data_id'])
        self.assertIn('name',response.json['data'])
        self.assertEqual(response.json['data']['name'],self.event['name'])

    def test_failed_event_get_one_event_not_found(self):
        url = '/events/' + str(self.event['data_id']+10)
        response = self.get_data(url)
        self.assertEqual(response.status_code,404)


class TestEventGetOneIncludeAccount(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        self.generate_event()
        self.generate_involvement()

    def test_success_event_get_all_include_account(self):
        query = {'include_account':True}
        url = '/events/' + str(self.event['data_id'])
        response = self.get_data(url,use_auth=True,query_string=query)
        self.assertEqual(response.status_code,200)
        self.assertIn('involved',response.json['data'])
        self.assertIsInstance(response.json['data']['involved'],list)
        self.assertIn('data_id',response.json['data']['involved'][0])
        self.assertIn('name',response.json['data']['involved'][0])
        self.assertEqual(response.json['data']['involved'][0]['event_id'],self.event['data_id'])
        self.assertEqual(response.json['data']['involved'][0]['account_id'],self.account['data_id'])
