from datetime import datetime, timedelta
from app.tests import BaseCase

class TestEventCreate(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()

    def test_success_event_create(self):
        start_time = datetime.utcnow() + timedelta(days=10)
        payload = {
            'name':'testevent',
            'shortname':'tv',
            'start':start_time.strftime('%d-%m-%Y %H:%M')
            }
        response = self.post_data('/events',payload)
        self.assertEqual(response.status_code,201)
        self.assertEqual(response.json['status'],True)
        self.assertIn('name',response.json['data'])
        self.assertEqual(response.json['data']['name'],payload['name'])
        self.assertIn('data_id',response.json['data'])
        self.assertIn('start',response.json['data'])
        self.assertEqual(response.json['data']['start'],payload['start'])

    def test_failed_event_create_no_token(self):
        start_time = datetime.utcnow() + timedelta(days=10)
        payload = {
            'name':'testevent',
            'shortname':'tv',
            'start':start_time.strftime('%d-%m-%Y %H:%M')
            }
        response = self.post_data('/events',payload,use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_event_create_empty_field(self):
        payload = {'name':'','start':''}
        response = self.post_data('/events',payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('name',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['name'])
        self.assertIn('start',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['start'])

    def test_failed_event_create_invalid_data(self):
        new_event = {
            'name':'New Event',
            'start':datetime.utcnow() + timedelta(days=10)
        }
        self.generate_event(data=new_event)
        endtime = new_event['start'] + timedelta(days=-1)
        payload = {
            'name':new_event['name'],
            'start':new_event['start'].strftime('%d-%m-%Y %H:%M'),
            'end':endtime.strftime('%d-%m-%Y %H:%M'),
        }
        response = self.post_data('/events',payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('name',response.json['detail_message'])
        self.assertIn(f'Name {payload["name"]} already exist.',response.json['detail_message']['name'])
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Start must be before End.',response.json['detail_message']['__all__'])