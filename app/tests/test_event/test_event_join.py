from datetime import datetime, timedelta

from app.tests import BaseCase
from app.models import EventInvolved

class TestEventJoin(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        self.generate_event()

    def test_success_join_event(self):
        url = '/events/' + str(self.event['data_id']) + '/join'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,200)
        involved_exist = False
        with self.app.app_context():
            joined_event = EventInvolved.query.filter(EventInvolved.event_id==self.event['data_id'],EventInvolved.account_id==self.account['data_id']).first()
            if joined_event:
                involved_exist = True
        self.assertTrue(involved_exist)

    def test_failed_join_event_no_token(self):
        url = '/events/' + str(self.event['data_id']) + '/join'
        response = self.post_data(url,{},use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_join_event_event_not_found(self):
        url = '/events/' + str(self.event['data_id']+10) + '/join'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,404)

    def test_failed_join_event_event_has_passed(self):
        new_event_data = {
            'name':'New Event',
            'start':datetime.utcnow() + timedelta(days=-2)
        }
        new_event = self.generate_event(data=new_event_data)
        url = '/events/' + str(new_event) + '/join'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Event has passed.',response.json['detail_message']['__all__'])
    
    def test_failed_join_event_event_has_been_joined(self):
        self.generate_involvement()
        url = '/events/' + str(self.event['data_id']) + '/join'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Event already joined.',response.json['detail_message']['__all__'])