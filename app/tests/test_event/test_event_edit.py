from datetime import datetime, timedelta

from app.tests import BaseCase

class TestEventEdit(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_event()
        self.generate_account()

    def test_success_event_edit(self):
        url = '/events/' + str(self.event['data_id'])
        payload = {'name':'Edited Event'}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.json['status'],True)
        self.assertIn('name',response.json['data'])
        self.assertEqual(response.json['data']['name'],payload['name'])

    def test_failed_event_edit_no_token(self):
        url = '/events/' + str(self.event['data_id'])
        payload = {'name':'Edited Event'}
        response = self.put_data(url,payload,use_auth=False)
        self.assertEqual(response.status_code,401)
        self.assertEqual(response.json['status'],False)

    def test_failed_event_edit_event_not_found(self):
        url = '/events/' + str(self.event['data_id']+10)
        payload = {'name':'Edited Event'}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,404)
        self.assertEqual(response.json['status'],False)
    
    def test_failed_event_edit_empty_field(self):
        url = '/events/' + str(self.event['data_id'])
        payload = {'name':'', 'start':''}
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('name',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['name'])
        self.assertIn('start',response.json['detail_message'])
        self.assertIn('Field may not be null.',response.json['detail_message']['start'])

    def test_failed_event_edit_invalid_data(self):
        new_event = {
            'name':'New Event',
            'start':datetime.utcnow() + timedelta(days=10)
        }
        self.generate_event(data=new_event)
        endtime = new_event['start'] + timedelta(days=-1)
        url = '/events/' + str(self.event['data_id'])
        payload = {
            'name':new_event['name'],
            'start':new_event['start'].strftime('%d-%m-%Y %H:%M'),
            'end':endtime.strftime('%d-%m-%Y %H:%M'),
        }
        response = self.put_data(url,payload)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('name',response.json['detail_message'])
        self.assertIn(f'Name {payload["name"]} already exist.',response.json['detail_message']['name'])
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Start must be before End.',response.json['detail_message']['__all__'])