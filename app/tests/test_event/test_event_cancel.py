from app.tests import BaseCase
from app.models import EventInvolved

class TestEventCancel(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        self.generate_event()
        self.generate_involvement()

    def test_success_cancel_event(self):
        url = '/events/' + str(self.event['data_id']) + '/cancel'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,200)
        involved_exist = True
        with self.app.app_context():
            joined_event = EventInvolved.query.filter(EventInvolved.event_id==self.event['data_id'],EventInvolved.account_id==self.account['data_id']).first()
            if not joined_event:
                involved_exist = False
        self.assertFalse(involved_exist)

    def test_failed_cancel_event_no_token(self):
        url = '/events/' + str(self.event['data_id']) + '/cancel'
        response = self.post_data(url,{},use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_cancel_event_event_not_found(self):
        url = '/events/' + str(self.event['data_id']+10) + '/cancel'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,404)

    def test_failed_cancel_event_event_not_yet_joined(self):
        with self.app.app_context():
            self.db.session.query(EventInvolved).filter(EventInvolved.data_id==self.involved['data_id']).delete()
            self.db.session.commit()
        url = '/events/' + str(self.event['data_id']) + '/cancel'
        response = self.post_data(url,{})
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('__all__',response.json['detail_message'])
        self.assertIn('Account not yet join event.',response.json['detail_message']['__all__'])