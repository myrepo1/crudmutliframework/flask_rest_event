from app.models import Event
from app.tests import BaseCase

class TestEventDelete(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_event()
        self.generate_account()

    def test_success_event_delete(self):
        url = '/events/' + str(self.event['data_id'])
        response = self.delete_data(url)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.json['status'],True)
        event_found = True
        with self.app.app_context():
            event = Event.query.filter(Event.data_id==self.event['data_id']).first()
            if not event:
                event_found = False
        self.assertFalse(event_found)

    def test_failed_event_delete_no_token(self):
        url = '/events/' + str(self.event['data_id'])
        response = self.delete_data(url, use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_event_delete_event_not_found(self):
        url = '/events/' + str(self.event['data_id']+10)
        response = self.delete_data(url)
        self.assertEqual(response.status_code,404)