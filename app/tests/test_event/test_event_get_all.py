from datetime import timedelta

from app.helpers.general import generate_utc_time
from app.tests import BaseCase

class TestEventGetAll(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_event()

    def test_success_event_get_all(self):
        response = self.get_data('/events')
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertIn('data_id',response.json['data'][0])
        self.assertIn('name',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['name'],self.event['name'])

class TestEventGetAllIncludeJoined(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        self.generate_event()
        self.generate_involvement()
    
    def test_success_event_get_all_include_joined(self):
        query = {'view_joined':True}
        response = self.get_data('/events',use_auth=True,query_string=query)
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertIn('data_id',response.json['data'][0])
        self.assertIn('involved',response.json['data'][0])
        self.assertIsInstance(response.json['data'][0]['involved'],list)
        self.assertIn('data_id',response.json['data'][0]['involved'][0])
        self.assertEqual(response.json['data'][0]['involved'][0]['event_id'],self.event['data_id'])
        self.assertEqual(response.json['data'][0]['involved'][0]['account_id'],self.account['data_id'])

    def test_failed_event_get_all_include_joined_no_token(self):
        query = {'view_joined':True}
        response = self.get_data('/events',query_string=query)
        self.assertEqual(response.status_code,200)
        self.assertIsInstance(response.json['data'],list)
        self.assertNotIn('involved',response.json['data'][0])

class TestEventGetAllWithQuery(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_event()
    
    def success_evet_get_all_with_query_equal(self):
        query = {'name':self.event['name']}
        more_event = {'name':'Test Event1','start':generate_utc_time()+timedelta(days=3)}
        self.generate_event(data=more_event)
        response = self.get_data('/accounts', query_string=query)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json['data'], list)
        self.assertEqual(len(response.json['data']),1)
        self.assertIn('name',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['name'],self.event['name'])

    def success_event_get_all_with_query_ilike(self):
        query = {'name__ilike':self.event['name']}
        more_event = {'name':'Test Event1','start':generate_utc_time()+timedelta(days=3)}
        self.generate_event(data=more_event)
        response = self.get_data('/accounts', query_string=query)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json['data'], list)
        self.assertEqual(len(response.json['data']),2)
        self.assertIn('name',response.json['data'][0])
        self.assertEqual(response.json['data'][0]['name'],self.event['name'])
        self.assertIn('name',response.json['data'][1])
        self.assertEqual(response.json['data'][1]['name'],more_event['name'])