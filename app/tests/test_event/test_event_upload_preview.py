from os import path
from urllib.parse import urlparse
from app.tests import BaseCase

class TestEventUploadPreview(BaseCase):
    def setUp(self):
        super().setUp()
        self.generate_account()
        self.generate_event()
        file_path = 'seeders/static/event.jpeg'
        root_path = self.app.root_path
        upper_root_path = '/'.join(root_path.split('/')[:-1])
        self.full_path = path.join(upper_root_path,file_path)
    
    def test_success_event_upload_preview(self):
        data = {'preview':open(self.full_path,'rb')}
        url = '/events/'+str(self.event['data_id'])+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.json['status'],True)
        self.assertIn('preview',response.json['data'])
        self.assertIsNotNone(response.json['data']['preview'])
        parse_result = urlparse(response.json['data']['preview'])
        self.assertNotEqual(parse_result.scheme,'')
        self.assertNotEqual(parse_result.netloc,'')

    def test_failed_event_upload_preview_no_token(self):
        data = {'preview':open(self.full_path,'rb')}
        url = '/events/'+str(self.event['data_id'])+'/image'
        response = self.post_non_json_data(url,data, use_auth=False)
        self.assertEqual(response.status_code,401)

    def test_failed_event_upload_preview_event_not_found(self):
        data = {'preview':open(self.full_path,'rb')}
        url = '/events/'+str(self.event['data_id']+10)+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,404)

    def test_failed_event_upload_preview_no_file(self):
        data = {'preview':''}
        url = '/events/'+str(self.event['data_id'])+'/image'
        response = self.post_non_json_data(url,data)
        self.assertEqual(response.status_code,400)
        self.assertIn('detail_message',response.json)
        self.assertIn('preview',response.json['detail_message'])
        self.assertIn('Preview Image Not Found.',response.json['detail_message']['preview'])