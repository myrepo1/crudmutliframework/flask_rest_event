"""
Create flask app and attach blueprint to it.
"""

from app import create_app
from app.resources.account import bp_api as account_api
from app.resources.event import bp_api as event_api
from app.resources.file import bp_api as file_api

def execute_app(app):
    """
    Register blueprint to flask app.
    """
    with app.app_context():
        app.register_blueprint(account_api.blueprint,url_prefix='/accounts')
        app.register_blueprint(event_api.blueprint,url_prefix='/events')
        app.register_blueprint(file_api.blueprint,url_prefix='/files')
        return app

def run_app():
    """
    Generate flask app.
    """
    app = create_app()
    return execute_app(app)

def run_test_app():
    """
    Generate flask app for testing.
    """
    app = create_app(is_test=True)
    return execute_app(app)
