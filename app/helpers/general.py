"""
General method helpers.
All uncategorized helpers written here.
"""
from datetime import datetime
from flask import request

def generate_utc_time():
    """
    Generate utc_time with second and microsecond equal to zero.
    """
    current_time = datetime.utcnow()
    current_time_dict = {
        'year':current_time.year,
        'month':current_time.month,
        'day':current_time.day,
        'hour':current_time.hour,
        'minute':current_time.minute
    }
    return datetime(**current_time_dict)

def check_field_exist(raw_field,columns):
    """
    Check if field exist in list of column names.
    Parameters:
    ------
    raw_field:
        Field to be checked.
    columns:
        List of table column's names.
    """
    field_list = raw_field.split('__')
    return field_list[0] in columns

def generate_field_filter(model,raw_field,value):
    """
    Generate SQlAlchemy filter argument with operator
    based on operator text in raw_field.
    Provided operators is 'gte','gt','lt','lte','like','in'.
    If operator is 'in', value must be a list.
    If raw_field has no operator or operator not included in provided operators,
    or operator is 'in' and value not a list, return equality operator.
    Parameters:
    -----
    model:
        SQLAlchemy table model.
    raw_field:str
        field name, optionally with its operator.
        Example: name__like, field's name is 'name', operator is 'like'.
    value:obj
        value to be compared in filter_argument, it could be string or int or other.
    """
    field_list = raw_field.split('__')
    field = getattr(model,field_list[0],None)
    if len(field_list)>1:
        if not isinstance(value,list):
            if field_list[1]=='gte':
                return field>=value
            elif field_list[1]=='gt':
                return field>value
            elif field_list[1]=='lt':
                return field<value
            elif field_list[1]=='lte':
                return field<=value
            elif field_list[1]=='like':
                return field.like(f"%{value}%")
            elif field_list[1]=='ilike':
                return field.ilike(f"%{value}%")
            elif field_list[1]=='ne':
                return field!=value
        elif field_list[1]=='in':
            return field.in_(value)
    return field==value

def generate_filter(model,query):
    """
    Generate list of filter to be used in SQLAlchemy query's filter method.
    Parameters:
    -----
    model:
        SQLAlchemy Table model.
    query: dict
        Dictionary of raw_field and value to be used in sql query.
    Returns: list
        Return list of filter arguments.
    """
    column_list = [item.name for item in model.__table__.columns]
    result = []
    for k,v in query.items():
        if check_field_exist(k,column_list):
            field_filter = generate_field_filter(model,k,v)
            result.append(field_filter)
    return result

def get_current_host():
    """
    Get current request's host_url.
    Return:str
    -----
        Host url.
    """
    return request.host_url
