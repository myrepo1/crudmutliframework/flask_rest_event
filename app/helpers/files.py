"""
This is module for save,load and delete files. All files are
stored in Config.MEDIA_DIR directory.
"""

from os import path,makedirs,remove
import uuid

from flask import send_from_directory, current_app as app

def generate_unique_name(filename):
    """
    Generate unique name using uuid4.
    Parameters:
    ------
    filename:str
        Original filename with its extension.
    Return: str
    -----
        Unique name with its file extension.
    """
    ext = filename.split('.')[-1]
    return uuid.uuid4().hex+'.'+ext

def check_file_exist(file_dir):
    """
    Check if file_dir path exist.
    If path is not absoulte path, it will be
    considered as subdirectory of Config.MEDIA_DIR path.
    Parameters:
    -----
    file_dir:str
        File directory to be checked.
    Return:bool
    -----
        Return True if path exist.
    """
    if path.isabs(file_dir):
        return path.exists(file_dir)
    else:
        media_dir = app.config.get('MEDIA_DIR')
        full_path = path.join(app.root_path,media_dir,file_dir)
        return path.exists(full_path)

def generate_dir(file_dir):
    """
    Generate directory inside Config.MEDIA_DIR.
    Parameters:
    -----
    file_dir:str
        File directory to be created inside Config.MEDIA_DIR.
    """
    media_dir = app.config.get('MEDIA_DIR')
    complete_path = path.join(app.root_path,media_dir,file_dir)
    if not check_file_exist(complete_path):
        makedirs(complete_path)

def generate_full_filepath(file_dir,filename=None):
    """
    Generate absolute path inside Config.MEDIA_DIR.
    Parameters:
    -----
    file_dir:str
        File directory, it could be folder directory or until its file.
    filename:bool
        If filename True, file_dir is considered as folder directory only.
    Returns:str
    -----
        Absolute path of requested directory.
    """
    media_dir = app.config.get('MEDIA_DIR')
    if filename:
        return path.join(app.root_path,media_dir,file_dir,filename)
    else:
        return path.join(app.root_path,media_dir,file_dir)

def save_file(file,file_dir,filename):
    """
    Save single file into disk. Create its directory, if not yet exist.
    Parameters:
    -----
    file:
        File to be saved.
    file_dir:str
        Directory for saving the file.
    filename:str
        Filename for saved file.
    Returns:str
        Relative path of file's location.
    """
    generate_dir(file_dir)
    fullpath = generate_full_filepath(file_dir,filename)
    file.save(fullpath)
    return path.join(file_dir,filename)

def load_file(file_dir,filename=None):
    """
    Load file from disk and send it as response.
    Parameters:
    -----
    file_dir:str
        File directory, it could be a folder directory or until its file.
    filename:bool
        If filename True, file_dir is considered as folder directory only.
    Returns:
    -----
        Response contain requested file.
    """
    if check_file_exist(file_dir):
        if filename:
            result_dir = path.join(app.config.get('MEDIA_DIR'),file_dir)
        else:
            dir_only,filename = file_dir.split('/')
            result_dir = path.join(app.config.get('MEDIA_DIR'),dir_only)
        return send_from_directory(result_dir,filename)
    else:
        return None

def remove_file(file_dir):
    """
    Delete single file from disk.
    Parameters:
    ----
    file_dir:str
        File directory in disk.
    """
    splitted_dir = file_dir.split('/')
    filename = splitted_dir[-1]
    container = splitted_dir[-2]    
    full_path = generate_full_filepath(container,filename=filename)
    try:
        remove(full_path)
    except OSError:
        return
