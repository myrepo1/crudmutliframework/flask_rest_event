unauthorized_response = {
    'code':403,
    'status':False,
    'message':'Unauthorized Access.'
}

resource_not_found = {
    'code':404,
    'status':False,
    'message':'Resource not Found.'
}

validation_failed = {
    'code':400,
    'status':False,
    'message':'Validation Failed.'
}