"""
Flask app generator, generate app and attach plugins to generated flask's app.
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.settings.config import Config, TestConfig
from app.settings.jwt import jwt

db = SQLAlchemy()
migrate = Migrate()


def create_app(is_test=False):
    """
    Create Flask app and init_app using those Flask app.
    Return:
    ------
    Flask App object.
    """
    app = Flask(__name__)
    if is_test:
        app.config.from_object(TestConfig)
    else:
        app.config.from_object(Config)
    db.init_app(app)
    migrate.init_app(app,db)
    jwt.init_app(app)
    return app
