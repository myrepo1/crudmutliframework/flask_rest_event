from datetime import datetime,timedelta
from flask import current_app
from os import path, makedirs
from shutil import rmtree, copyfile
from uuid import uuid4
from flask_migrate import downgrade, upgrade

from app.models import Event, Account, EventInvolved
from app import db

now = datetime.now()

file_url = 'http://localhost:5000/files/'

seed_data = {
    'events':[
        {
            'name':'Bazaar Rame Tahunan',
            'shortname':'bazaar20',
            'start':now+timedelta(days=5),
            'end':now+timedelta(days=8),
            'description':'Event bazaar tahunan. Banyak jualan disini.',
            'location':'Gedung Sate'
            },
        {
            'name':'Theater Perjuangan Kemerdekaan 1945',
            'shortname':'TP45',
            'start':now+timedelta(days=4),
            'end':now+timedelta(days=4,hours=2),
            'description':'Pertunjukan proses perjuangan kemerdekaan.',
            'location':'Gedung Merdeka'
            },
        {
            'name':'Lomba lari marathon se Bandung',
            'shortname':'maraBDG',
            'start':now+timedelta(days=-5),
            'end':now+timedelta(days=-5,hours=5),
            'description':'Lomba lari marathon se bandung.',
            'location':'Jalan Merdeka'
        },
        {
            'name':'Layar Tancap Bandung Tempo Doeloe',
            'shortname':'oldBDG',
            'start':now+timedelta(days=3),
            'end':now+timedelta(days=3,hours=3),
            'description':'Nonton bareng gambaran Bandung tempo dulu.',
            'location':'Museum Sri Baduga'
        },
        {
            'name':'1000 Pohon hijaukan Negeri',
            'shortname':'1000hijau',
            'start':now+timedelta(days=10),
            'description':'Menanam pohon bersama untuk penghijauan.',
            'location':'Babakan Siliwangi'
        }
    ],
    'accounts':[
        {
            'username':'admin',
            'is_staff':True,
            'password':'admin321_123',
            'name':'Event Admin',
            'phone':'08098912312'
        },
        {
            'username':'person1',
            'password':'person_321',
            'name':'Budi',
            'phone':'12930123213',
        },
    ]
}

def create_account(data):
    record = Account(**data)
    record.set_password(record.password)
    db.session.add(record)

def create_event(data):
    record = Event(**data)
    db.session.add(record)

def create_involvement(data):
    record = EventInvolved(**data)
    db.session.add(record)

def check_seed_data():
    if not path.exists('seeders/static/profpic.jpeg'):
        return False
    elif not path.exists('seeders/static/event.jpeg'):
        return False
    return True

def seed_account():
    print('Begin Seed Account')
    #TODO: Use Config to get media path
    if path.exists('app/media/profpic'):
        rmtree('app/media/profpic')
    makedirs('app/media/profpic')
    for item in seed_data['accounts']:
        profpic_filename = uuid4().hex+'.jpeg'
        copyfile('seeders/static/profpic.jpeg','app/media/profpic/'+profpic_filename)
        item['profpic'] = file_url+'profpic/'+profpic_filename
        create_account(item)
    print('End Seed Account')

def seed_event():
    print('Begin Seed Event')
    if path.exists('app/media/preview'):
        rmtree('app/media/preview')
    makedirs('app/media/preview')
    for item in seed_data['events']:
        preview_filename = uuid4().hex+'.jpeg'
        copyfile('seeders/static/event.jpeg','app/media/preview/'+preview_filename)
        item['preview'] = file_url + 'preview/'+preview_filename
        create_event(item)
    print('End Seed Event')

def seed_involvement():
    print('Begin Seed Involvement')
    event = Event.query.first()
    account = Account.query.filter(Account.is_staff==False).first()
    if not event or not account:
        print('Event or Account not found.')
        return
    create_involvement({'event_id':event.data_id,'account_id':account.data_id})
    print('End Seed Involvement')

def seed_db():
    if not check_seed_data():
        print('Required seed incomplete.')
        return
    with current_app.app_context():
        seed_account()
        seed_event()
        seed_involvement()
        db.session.commit()

def recreate_db():
    with current_app.app_context():
        db.drop_all()
        db.create_all()

def reset_db():
    with current_app.app_context():
        downgrade(directory='migrations',revision='base')
        upgrade(directory='migrations')

def recreate_and_seed_db():
    print('Begin Recreate Database')
    recreate_db()
    print('End Recreate Database')
    print('Begin Seed Database')
    seed_db()
    print('End Seed Database')

def reset_and_seed_db():
    print('Begin Reset Database')
    reset_db()
    print('End Reset Database')
    print('Begin Seed Database')
    seed_db()
    print('End Seed Database')